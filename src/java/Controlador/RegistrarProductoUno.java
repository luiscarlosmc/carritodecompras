

package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Producto;
import modelo.Tienda;
import ufps.util.SubirArchivoBean;

/**
 *
 * @author Oscar Molina 1151280 
 *         Luis Carlos Moreno 1151288
 */
@MultipartConfig
public class RegistrarProductoUno extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
        
             ServletContext sc = request.getSession().getServletContext();
             String dir = sc.getRealPath("").replace("\\build\\web", "\\web\\imagenes");;
          
               
        
            SubirArchivoBean subir=new SubirArchivoBean(request,dir,"archivo");
           
            subir.subirFile();
            String img=subir.getNombreArchivo();
            request.getSession().setAttribute("img",img);
            
            
           
             
             request.getRequestDispatcher("./jsp/registrarProducto.jsp").forward(request, response);
             } catch (Exception e) {
            request.getRequestDispatcher("./jsp/error.jsp").forward(request, response);
        }
             
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
