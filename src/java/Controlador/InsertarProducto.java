/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Blob;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import modelo.Producto;
import modelo.Tienda;
//import org.apache.el.stream.Stream;
import ufps.util.SubirArchivoBean;

/**
 *
 * @author Niver Romero
 */
@MultipartConfig
public class InsertarProducto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
            
         try{ 
            
            
             
             Tienda mytienda= (Tienda)(request.getSession().getAttribute("tienda"));
             String cantPro=String.valueOf(request.getSession().getAttribute("cantidadPro"));
             int cantidadPro=Integer.parseInt(cantPro);
             int codigo=Integer.parseInt(request.getParameter("codigo"));
             String nombre=request.getParameter("nombre");
             float precio=Float.parseFloat(request.getParameter("precio"));
             int cantidad=Integer.parseInt(request.getParameter("cantidad"));
             
             
               String img=String.valueOf(request.getSession().getAttribute("img")); 
             
             Producto producto= new Producto(codigo,nombre,precio,cantidad,img);
             
             
            if ( mytienda.insertarProducto(producto,cantidadPro)){
                request.getRequestDispatcher("./jsp/registrarProductoSi.jsp").forward(request, response);
                
                
                
            }  
            else{ 
                 request.getRequestDispatcher("./jsp/errorCanP.jsp").forward(request, response);
            }
            
         
           } catch (Exception e) {
            request.getRequestDispatcher("./jsp/error.jsp").forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
