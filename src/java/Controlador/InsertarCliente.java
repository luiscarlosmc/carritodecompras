/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Carrito;
import modelo.Cliente;
import modelo.Producto;
import modelo.Tienda;

/**
 *
 * @author Niver Romero
 */
public class InsertarCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
            
        try{
       
             Tienda mytienda= (Tienda)(request.getSession().getAttribute("tienda"));
            
             
            
             
             int cedula=Integer.parseInt(request.getParameter("cedula")); 
             String nombre=request.getParameter("nombre");
             String email=request.getParameter("email");
             
            
            LocalDate myFecha=LocalDate.now();
             
             
             Cliente cliente= new Cliente(cedula,nombre,email);
             
             if(mytienda.insertarCliente(cliente, cedula,email)){
                 
                 Carrito carrito=new Carrito(cedula,cliente, myFecha);
                 mytienda.crearcCarrito(carrito);
                 request.getSession().setAttribute("carrito",carrito );
                 request.getSession().setAttribute("cliente",cliente );
                request.getRequestDispatcher("./jsp/cliente.jsp").forward(request, response);  
            }else{
              request.getRequestDispatcher("./jsp/errorCliente.jsp").forward(request, response);
             }
             } catch (Exception e) {
            request.getRequestDispatcher("./jsp/error.jsp").forward(request, response);
        }
             
        }
            
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
