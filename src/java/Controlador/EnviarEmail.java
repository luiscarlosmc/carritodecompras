/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Carrito;
import modelo.Cliente;
import modelo.Tienda;
import ufps.util.Email;

/**
 *
 * @author Niver Romero
 */
public class EnviarEmail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try{
        Tienda mytienda= (Tienda)(request.getSession().getAttribute("tienda"));
        Carrito mycarrito=(Carrito) (request.getSession().getAttribute("carrito"));
        Cliente mycliente=(Cliente) (request.getSession().getAttribute("cliente"));
        
        System.out.println(" antes de enviar correo ");
        Email email=new Email();
        String mensaje=mycarrito.MostrarItemCorreo();
        String correoC=mycliente.getEmail();
        System.out.println("enviar correo "+mensaje+" "+correoC);
        String asunto="TIENDA EL PESO MENOS  FACTURA DE SUS PRODUCTOS  COMPRADOS";
        
        email.enviar("niverdromero12@gmail.com", "margaritaniver", correoC, mensaje, asunto);
        
       request.getRequestDispatcher("./jsp/cliente.jsp").forward(request, response); 
       } catch (Exception e) {
            request.getRequestDispatcher("./jsp/error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
