
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Carrito;
import modelo.Item;
import modelo.Producto;
import modelo.Tienda;

/**
 *
 * @author Oscar Molina 1151280 Luis Carlos Morena 1151288
 */
public class AgregarProductoCarrito extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            Tienda mytienda = (Tienda) (request.getSession().getAttribute("tienda"));
            Carrito mycarrito = (Carrito) (request.getSession().getAttribute("carrito"));

            int codigo = Integer.parseInt(request.getParameter("codigo"));
            int cantidad = Integer.parseInt(request.getParameter("cantidad"));

            Producto prod = mytienda.buscarProducot(codigo, cantidad);
            if (prod != null) {

                Item item = new Item(prod, cantidad);
                mycarrito.agregarItem(item, prod, cantidad);
                request.getRequestDispatcher("./jsp/cliente.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("./jsp/productoAgotado.jsp").forward(request, response);
            }

            request.getSession().setAttribute("carrito", mycarrito);

        } catch (Exception e) {
            request.getRequestDispatcher("./jsp/error.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
