   /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author estudiante
 */
public class Item  implements Comparable {
    
 private Producto myProducto;
 private int cantidad;

    public Item() {
    }

    public Item(Producto myProducto, int cantidad) {
        
        
        this.myProducto = myProducto;
        this.cantidad = cantidad;
    }
    
    public float  valorItem(){
       float valor=0;
       valor=this.myProducto.getPrecio()*this.cantidad;
        
        return valor;
    }

    public Producto getMyProducto() {
        return myProducto;
    }

    public void setMyProducto(Producto myProducto) {
        this.myProducto = myProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    @Override
    public int compareTo(Object o) {
        Item nue=(Item)o;
        return(this.myProducto.getCodigo()-nue.myProducto.getCodigo());
    }
    
}
