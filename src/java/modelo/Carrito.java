/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.TreeSet;
import java.time.LocalDate;

/**
 *
 * @author estudiante
 */
public class Carrito implements Comparable{
    
    private int id;
    private Cliente myCliente;
    private TreeSet<Item> items=new TreeSet();
    //Debe ser tomada del sistema
    private LocalDate fecha;

    public Carrito() {
    }
  
    public Carrito(int id, Cliente myCliente, LocalDate fecha) {
        this.id = id;
        this.myCliente = myCliente;
        this.fecha = fecha;
    }
    
    public void agregarItem(Item i, Producto p,int cantidad){
       
         if(!this.items.isEmpty()){
             for(Item item:this.getItems()){
                 if (item.getMyProducto().getCodigo()==p.getCodigo())
                 {
                   item.setCantidad(item.getCantidad()+cantidad);
                   return;
                 }
             }
         }
               
          
        this.items.add(i);
    }
    public String MostrarItemCorreo(){
       String cad="Factura\n"+"Fecha: "+this.getFecha()+"\n"+"Nombre: "+this.myCliente.getNombre()+"\n";
       cad+="Documento: "+this.myCliente.getCedula()+"\n"+"PRODUCTOS COMPRADOS\n";
       float suma=0;
         if(!this.items.isEmpty()){
             for(Item item:this.getItems()){
                 suma+=item.valorItem();
                 cad+="Producto: "+item.getMyProducto().getNombre()+" -> cant: "+item.getCantidad()+" -> precioUnid:  "+item.getMyProducto().getPrecio()+" -> total   "+item.valorItem()+"\n";
                
                 }
               
         }
               
        cad+="Total de la Compra= "+suma;  
     return cad;
    }
    public String MostrarItemFac(){
       String cad="";
       float suma=0;
         if(!this.items.isEmpty()){
             for(Item item:this.getItems()){
                 suma+=item.valorItem();
              cad+="Producto: "+item.getMyProducto().getNombre()+" -> cant: "+item.getCantidad()+" -> precioUnid:  "+item.getMyProducto().getPrecio()+" -> total   "+item.valorItem()+"<br>";
                
                 }
               
         }
               
        cad+="\n"+"Total de la Compra= "+suma;  
     return cad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getMyCliente() {
        return myCliente;
    }

    public void setMyCliente(Cliente myCliente) {
        this.myCliente = myCliente;
    }

    public TreeSet<Item> getItems() {
        return items;
    }

    public void setItems(TreeSet<Item> items) {
        this.items = items;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    
    @Override
    public int compareTo(Object o) {
        Carrito nue=(Carrito)o;
        return(this.id-nue.id);
    }
    
    
    
    public String getFecha2()
    {
        return this.fecha.getDayOfMonth()+"/"+this.fecha.getMonthValue()+"/"+this.fecha.getYear();
    
    }

    
    
    
    
    
    
    
}
