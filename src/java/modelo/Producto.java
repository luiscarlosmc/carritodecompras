/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.InputStream;
import java.sql.Blob;
//import org.apache.el.stream.Stream;

/**
 *
 * @author estudiante
 */
public class Producto  implements Comparable {
    
    private int codigo;
    private String nombre;
    private float precio;
    private int cantidad;
    private String  img;

   

    
    public Producto() {
    }

    public Producto(int codigo, String nombre, float precio, int cantidad, String img) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.img=img;
        
    }
    
    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
     @Override
    public int compareTo(Object o) {
       
      Producto des=(Producto)o;
      return (this.codigo-des.codigo);
        
    }
    
    
     public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
    @Override
    public String toString() {
        return "codigo= "  + codigo + "<br>" +"nombre= "+ nombre +"<br>" + "precio= " + precio + "<br>" + "cantidad= " + cantidad ;
    }
    
    
    
}
