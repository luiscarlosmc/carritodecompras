/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.TreeSet;



/**
 *((Clase contenedora))
 * @author estudiante
 */
public class Tienda {
    
    
    private  String nombre="El peso menos";
    private TreeSet<Producto> productos=new TreeSet();
    private TreeSet<Cliente> clientes=new TreeSet();
    private TreeSet<Carrito> carritos=new TreeSet();

    public Tienda() {
    }

    public boolean insertarProducto(Producto nuevo,int cantidad)
    {
        if (this.productos.size()<cantidad){
            this.productos.add(nuevo);
            return  true;
             
        }else{
         
        return false;
        }
    }
    
    public boolean insertarCliente( Cliente cliente,int cedula, String email) {
        
          if (!this.clientes.isEmpty()){
              for(Cliente cli:this.getClientes()){
                  if(cli.getCedula()==cedula  || cli.getEmail().equalsIgnoreCase(email)){
                      return false;
                  }
               
          }
          }
          this.clientes.add(cliente);
         return true;
    }
    
    public void crearcCarrito(Carrito car ) {
         this.carritos.add(car);
          
         
    }
    
     public Producto buscarProducot(int codigoP, int cantidad){
    
         
        
        if(!this.productos.isEmpty()){
            for(Producto pro:this.productos){
                if (pro.getCodigo()==codigoP && pro.getCantidad()>=cantidad){
          
                    pro.setCantidad(pro.getCantidad()-1);
                    return pro;
                }
                
            }
           
        }
         
         return null;
     }
    

    public TreeSet<Producto> getProductos() {
       
        return productos;
    }

    public void setProductos(TreeSet<Producto> productos) {
        this.productos = productos;
    }
    
    public TreeSet<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(TreeSet<Cliente> clientes) {
        this.clientes = clientes;
    }

    public TreeSet<Carrito> getCarritos() {
        return carritos;
    }

    public void setCarritos(TreeSet<Carrito> carritos) {
        this.carritos = carritos;
    }

    @Override
    public String toString() {
        return "Tienda{" + "nombre=" + nombre ;
    }

    public String getNombre() {
        return nombre;
    }

   
    
    
    
}
