<%-- 
    Document   : cantidadProducto
    Created on : 29/05/2019, 08:25:51 AM
    Author     : Oscar Molina 1151280, Luis Carlos Moreno 1151288
--%>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.*"%>


<html>

    <head>
        <title>Cantidad Producto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <meta name="keywords" content="Goggles a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script>
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
        <link href="css/login_overlay.css" rel='stylesheet' type='text/css' />
        <link href="css/style6.css" rel='stylesheet' type='text/css' />
        <link href="css/contact.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="css/shop.css" type="text/css" />
        <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all">
        <link rel="stylesheet" href="css/owl.theme.css" type="text/css" media="all">
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href="css/fontawesome-all.css" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800"
              rel="stylesheet">
    </head>
    <%

        Object myTienda = request.getSession().getAttribute("tienda");
        Tienda tienda = (Tienda) (myTienda);
        request.getSession().setAttribute("tienda", tienda);

    %>

    <body>
        <div class="banner-top container-fluid" id="home">
            <!-- header -->
            <header>
                <div class="row">

                    <div class="col-md-6 logo-w3layouts text-center">
                        <h1 class="logo-w3layouts">
                            <a class="navbar-brand" href="index.html">
                                TNT </a>		
                        </h1>
                    </div>

                    <div class="col-md-3 top-info-cart text-right mt-lg-4">
                        <ul class="cart-inner-info">
                            <li class="button-log">
                                <a class="btn-open" href="#">
                                    <span class="fa fa-user" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li class="galssescart galssescart2 cart cart box_1">
                                <form action="#" method="post" class="last">
                                    <input type="hidden" name="cmd" value="_cart">
                                    <input type="hidden" name="display" value="1">
                                    <button class="top_googles_cart" type="submit" name="submit" value="">
                                        Mi Carrito
                                        <i class="fas fa-cart-arrow-down"></i>
                                    </button>
                                </form>
                            </li>
                        </ul>
                        <!---->
                        <div class="overlay-login text-left">
                            <button type="button" class="overlay-close1">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                            <div class="wrap">
                                <h5 class="text-center mb-4">Inicia Secion</h5>
                                <div class="login p-5 bg-dark mx-auto mw-100">
                                    <form action="#" method="post">
                                        <div class="form-group">
                                            <label class="mb-2">Email: </label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="mb-2">Contraseña: </label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="" required="">
                                        </div>
                                        <div class="form-check mb-2">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                            <label class="form-check-label" for="exampleCheck1">Verificar</label>
                                        </div>
                                        <button type="submit" class="btn btn-primary submit mb-4">Iniciar Seccion</button>

                                    </form>
                                </div>
                                <!---->
                            </div>
                        </div>
                        <!---->
                    </div>
                </div>
                <div class="search">
                    <div class="mobile-nav-button">
                        <button id="trigger-overlay" type="button">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                    <!-- open/close -->
                    <div class="overlay overlay-door">
                        <button type="button" class="overlay-close">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <form action="#" method="post" class="d-flex">
                            <input class="form-control" type="search" placeholder="Search here..." required="">
                            <button type="submit" class="btn btn-primary submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>

                    </div>
                    <!-- open/close -->
                </div>
                <label class="top-log mx-auto"></label>
                <nav class="navbar navbar-expand-lg navbar-light bg-light top-header mb-2">

                    <button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">

                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav nav-mega mx-auto">
                            <li class="nav-item active">
                                <a class="nav-link ml-lg-0" href="index.html">Inicio
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <form action="IrRegistroProducto.do" method="post">
                                    <a class="nav-link" href="IrRegistroProducto.do" >Registrar Producto</a>
                                </form>
                            </li>


                        </ul>
                    </div>
                </nav>
            </header>
            <!-- //header -->
            <section class="banner-bottom-wthreelayouts py-lg-5 py-3">
                <div class="container">
                    <h3 class="tittle-w3layouts text-center my-lg-4 my-4">Cantiddad de Productos</h3>

                    <div class="contact_grid_right">
                        <form  action="RegistrarProducto.do" method="post"   name="Registrar Producto"  >
                            <div class="row contact_left_grid">
                                <div class="col-md-4 con-left">

                                </div>

                                <div class="col-md-4 con-left"> 
                                    <div class="form-group">


                                        <input class="form-control"  id="cant" type="number" min="1" name="c" placeholder="Ingresa un Numero" >
                                    </div>
                                    <div class="form-group">

                                        <input class="form-control" type="submit" value="Crear" >
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4 con-left">
                            </div>
                            <div class="col-md-4 con-right">



                            </div>


                        </form>
                    </div>
                </div>
            </section>
            <br>








            <!--footer -->
            <footer class="py-lg-3 py-3">
                <div class="container-fluid px-lg-5 px-3">
                    <div class="row footer-top-w3layouts">

                        <div class="col-lg-12 footer-grid-w3ls">
                            <div class="footer-title">
                                <h3>AUTOR</h3>
                            </div>
                            <div class="footer-text">
                                <p>NIVER DANIEL ROMERO--1151157</p><BR>
                                <p>UNIVERSIDAD FRANCISCO DE PAULA SANTANTDER</p>


                            </div>
                        </div>



                    </div>
                    <div class="copyright-w3layouts mt-4">
                        <p class="copy-right text-center ">&copy; 2019 Goggles. All Rights Reserved | Design by

                        </p>
                    </div>
                </div>
            </footer>
            <!-- //footer -->
            ;
            <script  type="text/javascript">

                function fijarcanttidad() {
                    var cantidad = document.getElementById('cant');



                    console.log("hola");
                    cantidad.setAttribute("readonly", "readonly");

                }

            </script>
            <script  type="text/javascript">

                function m() {
                    var mostrar = document.getElementById('imgP')
                    var file = document.querySelector('input[type=file]').files[0];

                    var leer = new FileReader();

                    if (file) {
                        leer.readAsDataURL(file);

                        leer.onloadend = function () {
                            mostrar.src = leer.result;
                        }
                        ;

                    } else {
                        mostrar.src = "";
                    }
                }

            </script>

            <!--jQuery-->
            <script src="js/jquery-2.2.3.min.js"></script>


            <script>
                $(document).ready(function () {
                    $("#myModal").modal();
                });
            </script>
            <!-- // modal -->

            <!--search jQuery-->
            <script src="js/modernizr-2.6.2.min.js"></script>
            <script src="js/classie-search.js"></script>
            <script src="js/demo1-search.js"></script>
            <!--//search jQuery-->
            <!-- cart-js -->
            <script src="js/minicart.js"></script>
            <script>
                googles.render();

                googles.cart.on('googles_checkout', function (evt) {
                    var items, len, i;

                    if (this.subtotal() > 0) {
                        items = this.items();

                        for (i = 0, len = items.length; i < len; i++) {
                        }
                    }
                });
            </script>
            <!-- //cart-js -->
            <script>
                $(document).ready(function () {
                    $(".button-log a").click(function () {
                        $(".overlay-login").fadeToggle(200);
                        $(this).toggleClass('btn-open').toggleClass('btn-close');
                    });
                });
                $('.overlay-close1').on('click', function () {
                    $(".overlay-login").fadeToggle(200);
                    $(".button-log a").toggleClass('btn-open').toggleClass('btn-close');
                    open = false;
                });
            </script>
            <!-- carousel -->
            <!-- Count-down -->
            <script src="js/simplyCountdown.js"></script>
            <link href="css/simplyCountdown.css" rel='stylesheet' type='text/css' />
            <script>
                var d = new Date();
                simplyCountdown('simply-countdown-custom', {
                    year: d.getFullYear(),
                    month: d.getMonth() + 2,
                    day: 25
                });
            </script>
            <!--// Count-down -->
            <script src="js/owl.carousel.js"></script>
            <script>
                $(document).ready(function () {
                    $('.owl-carousel').owlCarousel({
                        loop: true,
                        margin: 10,
                        responsiveClass: true,
                        responsive: {
                            0: {
                                items: 1,
                                nav: true
                            },
                            600: {
                                items: 2,
                                nav: false
                            },
                            900: {
                                items: 3,
                                nav: false
                            },
                            1000: {
                                items: 4,
                                nav: true,
                                loop: false,
                                margin: 20
                            }
                        }
                    })
                })
            </script>

            <!-- //end-smooth-scrolling -->


            <!-- dropdown nav -->
            <script>
                $(document).ready(function () {
                    $(".dropdown").hover(
                            function () {
                                $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                                $(this).toggleClass('open');
                            },
                            function () {
                                $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                                $(this).toggleClass('open');
                            }
                    );
                });
            </script>
            <!-- //dropdown nav -->
            <script src="js/move-top.js"></script>
            <script src="js/easing.js"></script>
            <script>
                    jQuery(document).ready(function ($) {
                        $(".scroll").click(function (event) {
                            event.preventDefault();
                            $('html,body').animate({
                                scrollTop: $(this.hash).offset().top
                            }, 900);
                        });
                    });
            </script>
            <script>
                $(document).ready(function () {
                    /*
                     var defaults = {
                     containerID: 'toTop', // fading element id
                     containerHoverID: 'toTopHover', // fading element hover id
                     scrollSpeed: 1200,
                     easingType: 'linear' 
                     };
                     */

                    $().UItoTop({
                        easingType: 'easeOutQuart'
                    });

                });
            </script>
            <!--// end-smoth-scrolling -->

            <script src="js/bootstrap.js"></script>
            <!-- js file -->


    </body>

</html>
