<%-- 
    Document   : factura
    Created on : 2/06/2019, 04:35:10 PM
    Author     : NOscar Molina 1151280, Luis Carlos Moreno 1151288
--%>
<@
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.*"%>
<!DOCTYPE html>


<head>
    <title>Factura</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Goggles a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
          Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="css/login_overlay.css" rel='stylesheet' type='text/css' />
    <link href="css/style6.css" rel='stylesheet' type='text/css' />
    <link href="css/contact.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/shop.css" type="text/css" />
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/owl.theme.css" type="text/css" media="all">
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800"
          rel="stylesheet">
</head>
<%
        
        
        
        Object myTienda=request.getSession().getAttribute("tienda");
        Tienda tienda=(Tienda)(myTienda);
        request.getSession().setAttribute("tienda", tienda);
        
        Object myCarrito=request.getSession().getAttribute("carrito");
        Carrito carrito=(Carrito)(myCarrito);
        request.getSession().setAttribute("carrito", carrito);
         
         
        String nombreC="";
        
        int documentoC=0;
        String cad="";
        
         for(Cliente cli:tienda.getClientes()){
                nombreC=cli.getNombre();
                documentoC=cli.getCedula();
         }
                
    
%>



<body>
    <div class="banner-top container-fluid" id="home">
        <!-- header -->
        <header>
            <div class="row">
                <div class="col-md-3 text-left mt-lg-1">
                    <img src="images/logo.png" class="img-fluid" alt="" id="logo">
                </div>
                <div class="col-md-6 logo-w3layouts text-center">
                    <h1 class="logo-w3layouts">
                        <a class="navbar-brand" href="#">
                            Industies TNT</a>		
                    </h1>
                </div>


            </div>



        </header>
        <!-- //header -->

        <section class="banner-bottom-wthreelayouts py-lg-5 py-3">
            <div class="container-fluid">
                <div class="inner-sec-shop px-lg-4 px-3">
                    <h3 class="tittle-w3layouts my-lg-4 my-4 text-center" >Factura</h3>

                    <!-- /womens -->
                    <div class="row">
                        <%
        
                      cad=carrito.MostrarItemFac();
                      String fecha=""+carrito.getFecha();
                        %>	
                        <div class="col-md-12"><h5 class="tittle-w3layouts my-lg-4 my-4 text-center" >Fecha: <%=fecha%></h5></div>
                        <div class="col-md-12"><h5 class="tittle-w3layouts my-lg-4 my-4 text-center" >Nombre: <%=nombreC%></h5></div>
                        <div class="col-md-12"><h5 class="tittle-w3layouts my-lg-4 my-4 text-center" >Documento: <%=documentoC%></h5></div>
                        <div class="col-md-12"><h4 class="tittle-w3layouts my-lg-4 my-4 text-center" >Productos comprados</h4></div>
                        <div class="col-md-12"><h5 class="tittle-w3layouts my-lg-4 my-4 text-center" ><%=cad%></h5></div>



                    </div>
                </div><br>

                </section>
                <div class="row contact_left_grid">
                    <div class="col-md-4 con-left">
                    </div>
                    <div class="col-md-4 con-right">

                        <input class="btn btn-primary submit" type="submit" value="Descargar" name=botonguardar" >


                    </div>
                    <form name="Enviar correo" action="EnviarEmail.do" method="post">     

                        <div class="col-md-4 con-right">

                            <input class="btn btn-primary submit" type="submit" onclick="confiEmail();" value="Enviar a Email" name=botonguardar" >



                        </div> 
                    </form>
                </div> <br>                                           




                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-center p-5 mx-auto mw-100">

                                <h3>Debes iniciar Seccion para agregar producto al carrito</h3>
                                <div class="login newsletter">
                                    <form action="#" >
                                        <div class="form-group">
                                            <label class="mb-2">Email: </label>
                                            <input type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Email" required="">
                                            <label class="mb-2">Contraseña: </label>
                                            <input type="password" class="form-control" id="exampleInputEmail2"  placeholder="Contraseña" required="">
                                        </div>
                                        <button type="submit" class="btn btn-primary submit mb-4">Entrar</button>
                                    </form>
                                    <form action="IrRegistroCliente.do" >

                                        <button type="submit" class="btn btn-primary submit mb-4">Registrar</button>
                                    </form>
                                    <p class="text-center">
                                        <a href="#"></a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--jQuery-->
                <script src="js/jquery-2.2.3.min.js"></script>
                <script  type="text/javascript">
                                function confiEmail() {

                                    alert("La factura se ha enviado a su email");


                                }
                </script>


                <script  type="text/javascript">
                    function modalR() {

                        $(document).ready(function () {
                            $("#myModal").modal();
                        });
                    }
                </script>
                <!-- // modal -->

                <!--search jQuery-->
                <script src="js/modernizr-2.6.2.min.js"></script>
                <script src="js/classie-search.js"></script>
                <script src="js/demo1-search.js"></script>
                <!--//search jQuery-->
                <!-- cart-js -->
                <script src="js/minicart.js"></script>
                <script>
                    googles.render();

                    googles.cart.on('googles_checkout', function (evt) {
                        var items, len, i;

                        if (this.subtotal() > 0) {
                            items = this.items();

                            for (i = 0, len = items.length; i < len; i++) {
                            }
                        }
                    });
                </script>
                <!-- //cart-js -->
                <script>
                    $(document).ready(function () {
                        $(".button-log a").click(function () {
                            $(".overlay-login").fadeToggle(200);
                            $(this).toggleClass('btn-open').toggleClass('btn-close');
                        });
                    });
                    $('.overlay-close1').on('click', function () {
                        $(".overlay-login").fadeToggle(200);
                        $(".button-log a").toggleClass('btn-open').toggleClass('btn-close');
                        open = false;
                    });
                </script>
                <!-- carousel -->
                <!-- Count-down -->
                <script src="js/simplyCountdown.js"></script>
                <link href="css/simplyCountdown.css" rel='stylesheet' type='text/css' />
                <script>
                    var d = new Date();
                    simplyCountdown('simply-countdown-custom', {
                        year: d.getFullYear(),
                        month: d.getMonth() + 2,
                        day: 25
                    });
                </script>
                <!--// Count-down -->
                <script src="js/owl.carousel.js"></script>
                <script>
                    $(document).ready(function () {
                        $('.owl-carousel').owlCarousel({
                            loop: true,
                            margin: 10,
                            responsiveClass: true,
                            responsive: {
                                0: {
                                    items: 1,
                                    nav: true
                                },
                                600: {
                                    items: 2,
                                    nav: false
                                },
                                900: {
                                    items: 3,
                                    nav: false
                                },
                                1000: {
                                    items: 4,
                                    nav: true,
                                    loop: false,
                                    margin: 20
                                }
                            }
                        })
                    })
                </script>

                <!-- //end-smooth-scrolling -->


                <!-- dropdown nav -->
                <script>
                    $(document).ready(function () {
                        $(".dropdown").hover(
                                function () {
                                    $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                                    $(this).toggleClass('open');
                                },
                                function () {
                                    $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                                    $(this).toggleClass('open');
                                }
                        );
                    });
                </script>
                <!-- //dropdown nav -->
                <script src="js/move-top.js"></script>
                <script src="js/easing.js"></script>
                <script>
                    jQuery(document).ready(function ($) {
                        $(".scroll").click(function (event) {
                            event.preventDefault();
                            $('html,body').animate({
                                scrollTop: $(this.hash).offset().top
                            }, 900);
                        });
                    });
                </script>
                <script>
                    $(document).ready(function () {
                        /*
                         var defaults = {
                         containerID: 'toTop', // fading element id
                         containerHoverID: 'toTopHover', // fading element hover id
                         scrollSpeed: 1200,
                         easingType: 'linear' 
                         };
                         */

                        $().UItoTop({
                            easingType: 'easeOutQuart'
                        });

                    });
                </script>
                <!--// end-smoth-scrolling -->

                <script src="js/bootstrap.js"></script>
                <!-- js file -->
                </body>

                </html>


