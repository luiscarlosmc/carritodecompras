<%-- 
    Document   : registrrarCliente
    Created on : 29/05/2019, 09:35:12 PM
    Author     : Oscar Molina 1151280, Luis Carlos Moreno 1151288
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="modelo.*"%>
<!DOCTYPE html>
<title>Registro de Cliente</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<script>
    addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
</script>
<link href="css/login_overlay.css" rel='stylesheet' type='text/css' />
<link href="css/style6.css" rel='stylesheet' type='text/css' />
<link href="css/contact.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/shop.css" type="text/css" />
<link rel="stylesheet" href="css/owl.theme.css" type="text/css" media="all">
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/fontawesome-all.css" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800"
      rel="stylesheet">
</head>

<%

    Object myTienda = request.getSession().getAttribute("tienda");
    Tienda tienda = (Tienda) (myTienda);
    request.getSession().setAttribute("tienda", tienda);


%>






<body>
    <div class="banner-top container-fluid" id="home">
        <header>
            <div class="row">
                <div class="col-md-6 logo-w3layouts text-center">
                    <h1 class="logo-w3layouts">
                        <a class="navbar-brand" href="#">
                            Industries TNT </a>		
                    </h1>
                </div>

                <div class="col-md-3 top-info-cart text-right mt-lg-4">
                    <ul class="cart-inner-info">
                        <li class="button-log">
                            <a class="btn-open" href="#">
                                <span class="fa fa-user" aria-hidden="true"></span>
                            </a>
                        </li>
                        <li class="galssescart galssescart2 cart cart box_1">
                            <form action="#" method="post" class="last">
                                <input type="hidden" name="cmd" value="_cart">
                                <input type="hidden" name="display" value="1">
                                <button class="top_googles_cart" type="submit" name="submit" value="">
                                    Mi Carro
                                    <i class="fas fa-cart-arrow-down"></i>
                                </button>
                            </form>
                        </li>
                    </ul>
                    <div class="overlay-login text-left">
                        <button type="button" class="overlay-close1">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </button>
                        <div class="wrap">
                            <h5 class="text-center mb-4">Inicia Sesion</h5>
                            <div class="login p-5 bg-dark mx-auto mw-100">
                                <form action="#" method="post">
                                    <div class="form-group">
                                        <label class="mb-2">Email: </label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required="">
                                    </div>
                                    <div class="form-group">
                                        <label class="mb-2">Contraseña: </label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="" required="">
                                    </div>
                                    <div class="form-check mb-2">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Tocame</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary submit mb-4">Registrarse</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search">
                <div class="mobile-nav-button">
                    <button id="trigger-overlay" type="button">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
                <div class="overlay overlay-door">
                    <button type="button" class="overlay-close">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </button>
                    <form action="#" method="post" class="d-flex">
                        <input class="form-control" type="search" placeholder="busqueda..." required="">
                        <button type="submit" class="btn btn-primary submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>

                </div>
            </div>
            <label class="top-log mx-auto"></label>
            <nav class="navbar navbar-expand-lg navbar-light bg-light top-header mb-2">

                <button class="navbar-toggler mx-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">

                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav nav-mega mx-auto">
                        <li class="nav-item active">
                            <a class="nav-link ml-lg-0" href="MostrarTienda.do">Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>

                    </ul>

                </div>
            </nav>
        </header>

        <section class="banner-bottom-wthreelayouts py-lg-5 py-3">
            <div class="container">
                <h3 class="tittle-w3layouts text-center my-lg-4 my-4">Registrate</h3>

                <div class="contact_grid_right">
                    <form name="Inserta Cliente" action="InsertarCliente.do" method="post" >


                        <div class="row contact_left_grid">
                            <div class="col-md-3 con-left">

                            </div>
                            <div class="col-md-6 con-left">
                                <div class="form-group">
                                    <label class="my-2">Cedula: </label>
                                    <input class="form-control" type="number" min="1" name="cedula" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Nombre: </label>
                                    <input class="form-control" type="text" name="nombre" placeholder="" required="">
                                </div>
                                <div class="form-group">
                                    <label class="my-2">Email: </label>
                                    <input class="form-control" type="email" name="email" placeholder="" required="">

                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="submit" value="Registrarme" >
                                </div>
                            </div>




                        </div>
                    </form>

                </div>
            </div>
        </section>




        <footer class="py-lg-3 py-3">
            <div class="container-fluid px-lg-5 px-3">
                <div class="row footer-top-w3layouts">

                    <div class="col-lg-12 footer-grid-w3ls">
                        <div class="footer-title">
                            <h3>AUTOR</h3>
                        </div>
                        <div class="footer-text">
                            <p>Luis Carlos Moreno Calderon 1151288</p><BR>
                            <p>Oscar Alejandro Molina Arce 1151280</p><BR>
                            <p>Universidad Francisco de Paula Santander</p>


                        </div>
                    </div>



                </div>
                <div class="copyright-w3layouts mt-4">

                </div>
            </div>
        </footer>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body text-center p-5 mx-auto mw-100">

                        <h3>inicia Sesion para agregar producto al carrito</h3>
                        <div class="login newsletter">
                            <form action="#" >
                                <div class="form-group">
                                    <label class="mb-2">Email: </label>
                                    <input type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp" placeholder="Email" required="">
                                    <label class="mb-2">Contraseña: </label>
                                    <input type="password" class="form-control" id="exampleInputEmail2"  placeholder="Contraseña" required="">
                                </div>
                                <button type="submit" class="btn btn-primary submit mb-4">Entrar</button>
                            </form>
                            <form action="IrRegistroCliente.do" >


                                <button type="submit" class="btn btn-primary submit mb-4">Registrarme</button>
                            </form>
                            <p class="text-center">
                                <a href="#"></a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <script src="js/jquery-2.2.3.min.js"></script>


        <script  type="text/javascript">
    function modalR() {

        $(document).ready(function () {
            $("#myModal").modal();
        });
    }
        </script>

        <script src="js/modernizr-2.6.2.min.js"></script>
        <script src="js/classie-search.js"></script>
        <script src="js/demo1-search.js"></script>
        <script src="js/minicart.js"></script>
        <script>
    googles.render();

    googles.cart.on('googles_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
            }
        }
    });
        </script>
        <script>
            $(document).ready(function () {
                $(".button-log a").click(function () {
                    $(".overlay-login").fadeToggle(200);
                    $(this).toggleClass('btn-open').toggleClass('btn-close');
                });
            });
            $('.overlay-close1').on('click', function () {
                $(".overlay-login").fadeToggle(200);
                $(".button-log a").toggleClass('btn-open').toggleClass('btn-close');
                open = false;
            });
        </script>

        <script src="js/simplyCountdown.js"></script>
        <link href="css/simplyCountdown.css" rel='stylesheet' type='text/css' />
        <script>
            var d = new Date();
            simplyCountdown('simply-countdown-custom', {
                year: d.getFullYear(),
                month: d.getMonth() + 2,
                day: 25
            });
        </script>

        <script>
            $(document).ready(function () {
                $(".dropdown").hover(
                        function () {
                            $('.dropdown-menu', this).stop(true, true).slideDown("fast");
                            $(this).toggleClass('open');
                        },
                        function () {
                            $('.dropdown-menu', this).stop(true, true).slideUp("fast");
                            $(this).toggleClass('open');
                        }
                );
            });
        </script>
        <script src="js/move-top.js"></script>
        <script src="js/easing.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 900);
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear' 
                 };
                 */

                $().UItoTop({
                    easingType: 'easeOutQuart'
                });

            });
        </script>

        <script src="js/bootstrap.js"></script>
</body>

</html>

